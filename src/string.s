.data
mark1: .fill 2, 1, 0
mark2: .fill 2, 1, 0
mark3: .fill 2, 1, 0

line: .string " %s | %s | %s \n"

.text
.global main
.func main

main:
	push {lr}

	mov r9, #0

	mov r0, #'X'
	ldr r1, =mark1
	strb r0, [r1]
	strb r9, [r1, #1]

	mov r0, #' '
	ldr r1, =mark2
	strb r0, [r1]
	strb r9, [r1, #1]

	mov r0, #'O'
	ldr r1, =mark3
	strb r0, [r1]
	strb r9, [r1, #1]

	ldr r0,=line
	ldr r1, =mark1
	ldr r2, =mark2
	ldr r3, =mark3

	bl printf

	pop {lr}
	bx lr
