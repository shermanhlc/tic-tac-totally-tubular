.data
title: .string  "
 _________  ___  ________          _________  ________  ________              
|\\___   ___\\\\  \\|\\   ____\\        |\\___   ___\\\\   __  \\|\\   ____\\             
\\|___ \\  \\_\\ \\  \\ \\  \\___|        \\|___ \\  \\_\\ \\  \\|\\  \\ \\  \\___|             
     \\ \\  \\ \\ \\  \\ \\  \\                \\ \\  \\ \\ \\   __  \\ \\  \\                
      \\ \\  \\ \\ \\  \\ \\  \\____            \\ \\  \\ \\ \\  \\ \\  \\ \\  \\____           
       \\ \\__\\ \\ \\__\\ \\_______\\           \\ \\__\\ \\ \\__\\ \\__\\ \\_______\\         
        \\|__|  \\|__|\\|_______|            \\|__|  \\|__|\\|__|\\|_______|         
                                                                              
                                                                              
                                                                              
 _________  ________  _________  ________  ___       ___           ___    ___ 
|\\___   ___\\\\   __  \\|\\___   ___\\\\   __  \\|\\  \\     |\\  \\         |\\  \\  /  /|
\\|___ \\  \\_\\ \\  \\|\\  \\|___ \\  \\_\\ \\  \\|\\  \\ \\  \\    \\ \\  \\        \\ \\  \\/  / /
     \\ \\  \\ \\ \\  \\\\\\  \\   \\ \\  \\ \\ \\   __  \\ \\  \\    \\ \\  \\        \\ \\    / / 
      \\ \\  \\ \\ \\  \\\\\\  \\   \\ \\  \\ \\ \\  \\ \\  \\ \\  \\____\\ \\  \\____    \\/  /  /  
       \\ \\__\\ \\ \\_______\\   \\ \\__\\ \\ \\__\\ \\__\\ \\_______\\ \\_______\\__/  / /    
        \\|__|  \\|_______|    \\|__|  \\|__|\\|__|\\|_______|\\|_______|\\___/ /     
                                                                 \\|___|/      
                                                                              
                                                                              
 _________  ___  ___  ________  ___  ___  ___       ________  ________        
|\\___   ___\\\\  \\|\\  \\|\\   __  \\|\\  \\|\\  \\|\\  \\     |\\   __  \\|\\   __  \\       
\\|___ \\  \\_\\ \\  \\\\\\  \\ \\  \\|\\ /\\ \\  \\\\\\  \\ \\  \\    \\ \\  \\|\\  \\ \\  \\|\\  \\      
     \\ \\  \\ \\ \\  \\\\\\  \\ \\   __  \\ \\  \\\\\\  \\ \\  \\    \\ \\   __  \\ \\   _  _\\     
      \\ \\  \\ \\ \\  \\\\\\  \\ \\  \\|\\  \\ \\  \\\\\\  \\ \\  \\____\\ \\  \\ \\  \\ \\  \\\\  \\|    
       \\ \\__\\ \\ \\_______\\ \\_______\\ \\_______\\ \\_______\\ \\__\\ \\__\\ \\__\\\\ _\\    
        \\|__|  \\|_______|\\|_______|\\|_______|\\|_______|\\|__|\\|__|\\|__|\\|__|
\n"   

.equ rest, 1
.equ long_rest, 4
newline: .string "\n"

author_string_top: .string "                       developed by:                      \n"
author_string_bot: .string "James Jensen, Lydia Miller, Sam Sherman, Andrew Vonderhaar\n"

course: .string            "# # #                  ee.3780                       # # #\n"
version: .string           "                       version:                           \n"
semester: .string          "                       fall.2023                          \n"

.text
.global main
.func main

main:
    push {lr}
    bl print_intro
    pop {lr}
    
    bx lr

print_intro:
    push {r4-r12, lr}

    bl clear_terminal           @ clear terminal

    ldr r0, =rest
    bl sleep

    ldr r0, =title              @ prints ascii art text
    bl printf
    
    mov r0, #long_rest
    bl sleep

    ldr r0, =author_string_top  @ print author strings
    bl printf

    mov r0, #rest
    bl sleep

    ldr r0, =author_string_bot
    bl printf

    mov r0, #rest
    bl sleep

    ldr r0, =course              @ print course info
    bl printf

    mov r0, #rest
    bl sleep

    ldr r0, =version             @ print version and semester
    bl printf

    ldr r0, =semester
    bl printf

    bl clear_terminal

    ldr r0, #long_rest
    bl sleep

    pop {r4-r12, lr}
    bx lr

@ parameters:
    @ none
@ returns:
    @ none
clear_terminal:
        push {r4-r12, lr}

        mov r0, #1                  @ fd: stdout
        ldr r1, =clear_screen       
        ldr r2, =4                  
        mov r7, #4                  @ syscall: write
        svc 0

        pop {r4-r12, lr}
        bx lr
