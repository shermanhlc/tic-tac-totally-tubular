.equ MAX_INPUT_CHARS, 128

.equ rest, 1
.equ long_rest, 2

.data
@@ GAME VALUES
    @ array representing the board
        @ 0 = empty
        @ 1 = player 1 (X)
        @ 2 = player 2 (O)
    board_array: .word 0, 0, 0, 0, 0, 0, 0, 0, 0
    turn_counter: .word 0  @ counts the number of turns
    player_turn: .word 1   @ 1 = player 1, 2 = player 2
    input_num: .word 0     @ value user inputs

@@ BOARD PRINTING MEMORY
    horizontal_line: .string "---|---|---\n"
    .balign 4

    vertical_pipe: .string "|"
    newline: .string "\n"

    mark: .space 4
    red_start:  .asciz "\033[38;5;208m"  @ orange
    red_end:    .asciz "\033[0m"
    blue_start: .asciz "\033[34m"        @ blue
    blue_end:   .asciz "\033[0m"
    .balign 4

@@ USER INPUT MEMORY
    player_input_buffer: .space 16
    non_number_dump: .space MAX_INPUT_CHARS
    .balign 4

    scan_format_parse: .string "%d %s"
    .balign 4

@@ PROMPTS AND ERRORS
    @prompts
    turn_prompt: .string "Player %d, enter the number of an avaliable board space: \n"
    win_prompt: .string "Game over! Player %d wins!\n"
    tie_prompt: .string "Game over! Tie game!\n"

    @ errors
    input_error: .string "ERR: invalid input.\n"
    .balign 4
    clear_screen: .string "\033[2J"
    .balign 4

@@ TITLE
title: .string  "
 _________  ___  ________          _________  ________  ________              
|\\___   ___\\\\  \\|\\   ____\\        |\\___   ___\\\\   __  \\|\\   ____\\             
\\|___ \\  \\_\\ \\  \\ \\  \\___|        \\|___ \\  \\_\\ \\  \\|\\  \\ \\  \\___|             
     \\ \\  \\ \\ \\  \\ \\  \\                \\ \\  \\ \\ \\   __  \\ \\  \\                
      \\ \\  \\ \\ \\  \\ \\  \\____            \\ \\  \\ \\ \\  \\ \\  \\ \\  \\____           
       \\ \\__\\ \\ \\__\\ \\_______\\           \\ \\__\\ \\ \\__\\ \\__\\ \\_______\\         
        \\|__|  \\|__|\\|_______|            \\|__|  \\|__|\\|__|\\|_______|         
                                                                              
                                                                              
                                                                              
 _________  ________  _________  ________  ___       ___           ___    ___ 
|\\___   ___\\\\   __  \\|\\___   ___\\\\   __  \\|\\  \\     |\\  \\         |\\  \\  /  /|
\\|___ \\  \\_\\ \\  \\|\\  \\|___ \\  \\_\\ \\  \\|\\  \\ \\  \\    \\ \\  \\        \\ \\  \\/  / /
     \\ \\  \\ \\ \\  \\\\\\  \\   \\ \\  \\ \\ \\   __  \\ \\  \\    \\ \\  \\        \\ \\    / / 
      \\ \\  \\ \\ \\  \\\\\\  \\   \\ \\  \\ \\ \\  \\ \\  \\ \\  \\____\\ \\  \\____    \\/  /  /  
       \\ \\__\\ \\ \\_______\\   \\ \\__\\ \\ \\__\\ \\__\\ \\_______\\ \\_______\\__/  / /    
        \\|__|  \\|_______|    \\|__|  \\|__|\\|__|\\|_______|\\|_______|\\___/ /     
                                                                 \\|___|/      
                                                                              
                                                                              
 _________  ___  ___  ________  ___  ___  ___       ________  ________        
|\\___   ___\\\\  \\|\\  \\|\\   __  \\|\\  \\|\\  \\|\\  \\     |\\   __  \\|\\   __  \\       
\\|___ \\  \\_\\ \\  \\\\\\  \\ \\  \\|\\ /\\ \\  \\\\\\  \\ \\  \\    \\ \\  \\|\\  \\ \\  \\|\\  \\      
     \\ \\  \\ \\ \\  \\\\\\  \\ \\   __  \\ \\  \\\\\\  \\ \\  \\    \\ \\   __  \\ \\   _  _\\     
      \\ \\  \\ \\ \\  \\\\\\  \\ \\  \\|\\  \\ \\  \\\\\\  \\ \\  \\____\\ \\  \\ \\  \\ \\  \\\\  \\|    
       \\ \\__\\ \\ \\_______\\ \\_______\\ \\_______\\ \\_______\\ \\__\\ \\__\\ \\__\\\\ _\\    
        \\|__|  \\|_______|\\|_______|\\|_______|\\|_______|\\|__|\\|__|\\|__|\\|__|
\n"   


author_string_top: .string "                       developed by:                      \n"
author_string_bot: .string "James Jensen, Lydia Miller, Sam Sherman, Andrew Vonderhaar\n"

course: .string            "# # #                  ee.3780                       # # #\n"
version: .string           "                       version:                           \n"
semester: .string          "                       fall.2023                          \n"
@@

.text
.global main
.func main

main:
    push {lr}

    title_screen:
        bl print_intro
        bl clear_terminal

    main_loop:
        bl turn         @ run a turn

        cmp r0, #0
        beq main_loop   @ if game is not over, continue loop

    game_over:
        cmp r0, #2
        beq game_over_tie

        game_over_win:
            bl update_board
            bl print_board_schizo

            ldr r0, =player_turn  @ r0 = player
            ldr r1, [r0]

            ldr r0, =win_prompt  @ print game over and winning player
            bl printf

            b _end
        game_over_tie:
            @bl update_boards @ THIS WAS MISSPELLED
    
            bl print_board_schizo

            ldr r0, =tie_prompt   @ print tie game prompt
            bl printf

    _end:
        pop {lr} 
        bx lr

@ parameters:
    @ none
    @ note: print_board should be called before the first call of turn

@ returns:
    @ r0 = 1 if game is over, 2 if tied, 0 otherwise
turn:
    push {r4-r12, lr}

    bl print_board_schizo

    input_test:
    bl get_user_input  @ get the user input
    bl update_board    @ update the board

    bl check_winner  @ check for winner
    cmp r0, #1
    moveq r0, #1
    beq turn_end

    switch_player:
        ldr r0, =player_turn
        ldr r1, [r0]

        cmp r1, #1             
        moveq r1, #2           @ if r1 = 1, set r1 = 2
        movne r1, #1           @ if r1 = 2, set r1 = 1
        str r1, [r0]           @ save to player_turn


    increment_turn:
        ldr r0, =turn_counter
        ldr r1, [r0]

        add r1, #1             @ increment turn counter
        str r1, [r0]           @ store turn counter

        cmp r1, #8
        movgt r0, #2
        bgt turn_end
    
    continue_game:
        mov r0, #0             @ if no winner is found and there are still turns, r0 = 0

    turn_end:
        pop {r4-r12, lr}
        bx lr

get_user_input:
    push {r4-r12, lr}

    bl clear_buffer
    prompt_player:
        ldr r2, =player_turn
        ldr r1, [r2]          @ load player turn to r1
        mov r2, #0

        ldr r0, =turn_prompt
        bl printf             @ print turn prompt
@ matt says print the board here (he's wrong)
    read_input:
	    mov r0, #0
        ldr r1, =player_input_buffer   @ load user input to buffer
        mov r2, #(MAX_INPUT_CHARS - 1)
        mov r7, #3                     @ syscall: read
        svc 0

    parse:
        ldr r0, =player_input_buffer   @ load user input
        ldr r1, =scan_format_parse
        ldr r2, =input_num             @ save the desired integer
        ldr r3, =non_number_dump       @ dump non-numbers

        bl sscanf                      @ parse input

    check_input_length:
        cmp r0, #1                     @ check if sscanf returned 1
        ldrne r0, =input_error
        blne printf                    @ print error message

        bne prompt_player              @ if sscanf did not return 1, branch to prompt_player

    check_input_call:
        bl check_input
        cmp r0, #0
        blt prompt_player              @ if check_input returned -1, branch to prompt_player

    bl clear_terminal
    pop {r4-r12, lr}
    bx lr

@parameters:
    @ none
@ returns:
    @ none
print_board_schizo:
    push {lr}

    mov r8, #0
    mov r9, #0

    print_board_loop:
        
        mov r7, #0

        mark_printing_loop:
            ldr r0, =board_array
            ldr r1, [r0, r9, lsl #2]
            mov r2, r9
            bl print_mark
            test:
            
            add r9, #1
            add r7, #1

            cmp r7, #3
            ldrlt r0, =vertical_pipe
            bllt printf
            aft:
            cmp r7, #3
            blt mark_printing_loop

        ldr r0, =newline
        bl printf
        cmp r8, #2
        bge jump_horizontal_line

        ldr r0, =horizontal_line
        bl printf

        jump_horizontal_line:
            add r8, #1
            cmp r8, #2
            ble print_board_loop

    pop {lr}
    bx lr

@ parameters:
    @ r1 = board value
    @ r2 = counter
@ returns: 
    @ none
print_mark:
    push {lr}

    ldr r0, =mark
    cmp r1, #0
    beq zero

    cmp r1, #1
    beq x

    cmp r1, #2
    beq o

    zero:  @ empty mark
        mov r3, #' '
        strb r3, [r0]      @ add space
        add r1, r2, #'0'   @ set r1 to ascii value of counter
        strb r1, [r0, #1]  @ store ascii char to mark
        strb r3, [r0, #2]

        bl printf         @ print mark in white
        b print_mark_end

    x:
        mov r10, #'X'
        strb r10, [r0, #1]      @ store ascii char to mark

        mov r3, #' '
        strb r3, [r0]
        strb r3, [r0, #2]

        ldr r0, =red_start  @ set text color to red
        bl printf

        ldr r0, =mark       @ print mark in red
        bl printf

        ldr r0, =red_end    @ set text color to default
        bl printf

        b print_mark_end

    o:
        mov r10, #'O'
        strb r10, [r0, #1]

        mov r3, #' '
        strb r3, [r0]
        strb r3, [r0, #2]

        ldr r0, =blue_start  @ set text color to blue
        bl printf

        ldr r0, =mark        @ print mark in blue
        bl printf

        ldr r0, =blue_end    @ set text color to default
        bl printf

        b print_mark_end

    print_mark_end:
        pop {lr}
        bx lr

@ parameters:
    @ none
@ returns: 
    @ none
print_intro:
    push {r4-r12, lr}

    bl clear_terminal           @ clear terminal

    ldr r0, =rest
    bl sleep

    ldr r0, =title              @ prints ascii art text
    bl printf
    
    mov r0, #long_rest
    bl sleep

    ldr r0, =author_string_top  @ print author strings
    bl printf

    mov r0, #rest
    bl sleep

    ldr r0, =author_string_bot
    bl printf

    mov r0, #rest
    bl sleep

    ldr r0, =course              @ print course info
    bl printf

    mov r0, #rest
    bl sleep

    ldr r0, =version             @ print version and semester
    bl printf

    ldr r0, =semester
    bl printf

    mov r0, #long_rest
    bl sleep

    bl clear_terminal

    mov r0, #long_rest
    bl sleep

    pop {r4-r12, lr}
    bx lr

@ parameters:
    @ r0 = board array address

@ returns:
    @ r0 = 1 if winner is found, 0 otherwise
check_winner:
    push {lr}
    ldr r0, =board_array

    horizontal_check_1:
        ldr r1, [r0]      @ index: 0
        ldr r2, [r0, #4]  @ 1
        ldr r3, [r0, #8]  @ 2

        cmp r1, #0
        beq horizontal_check_2

        @ comparing via transitive property
        cmp r1, r2
        bne horizontal_check_2
        cmp r1, r3
        beq winner_found
    
    horizontal_check_2:
        ldr r1, [r0, #12]  @ 3
        ldr r2, [r0, #16]  @ 4
        ldr r3, [r0, #20]  @ 5

        cmp r1, #0
        beq horizontal_check_3

        @ comparing via transitive property
        cmp r1, r2
        bne horizontal_check_3
        cmp r1, r3
        beq winner_found

    horizontal_check_3:
        ldr r1, [r0, #24]  @ 6
        ldr r2, [r0, #28]  @ 7
        ldr r3, [r0, #32]  @ 8

        cmp r1, #0
        beq vertical_check_1

        @ comparing via transitive property
        cmp r1, r2
        bne vertical_check_1
        cmp r1, r3
        beq winner_found

    vertical_check_1:
        ldr r1, [r0]       @ 0
        ldr r2, [r0, #12]  @ 3
        ldr r3, [r0, #24]  @ 6

        cmp r1, #0
        beq vertical_check_2

        @ comparing via transitive property
        cmp r1, r2
        bne vertical_check_2
        cmp r1, r3
        beq winner_found
    
    vertical_check_2:
        ldr r1, [r0, #4]   @ 1
        ldr r2, [r0, #16]  @ 4
        ldr r3, [r0, #28]  @ 7

        cmp r1, #0
        beq vertical_check_3

        @ comparing via transitive property
        cmp r1, r2
        bne vertical_check_3
        cmp r1, r3
        beq winner_found

    vertical_check_3:
        ldr r1, [r0, #8]   @ 2
        ldr r2, [r0, #20]  @ 5
        ldr r3, [r0, #32]  @ 8

        cmp r1, #0
        beq diagonal_check_1

        @ comparing via transitive property
        cmp r1, r2
        bne diagonal_check_1
        cmp r1, r3
        beq winner_found

    diagonal_check_1:
        ldr r1, [r0]       @ 0
        ldr r2, [r0, #16]  @ 4
        ldr r3, [r0, #32]  @ 8

        cmp r1, #0
        beq diagonal_check_2

        @ comparing via transitive property
        cmp r1, r2
        bne diagonal_check_2
        cmp r1, r3
        beq winner_found

    diagonal_check_2:
        ldr r1, [r0, #8]   @ 2
        ldr r2, [r0, #16]  @ 4
        ldr r3, [r0, #24]  @ 6

        cmp r1, #0
        beq no_winner_found

        @ comparing via transitive property
        cmp r1, r2
        bne no_winner_found
        cmp r1, r3
        beq winner_found
    
    no_winner_found:
        mov r0, #0
        b check_winner_exit

    winner_found:
        mov r0, #1
        b check_winner_exit

    check_winner_exit:
        pop {lr}
        bx lr



@ parameters:
    @ none

@ returns:
    @ none
update_board:
    push {r4-r12, lr}

    ldr r1, =input_num    @ load addresses
    ldr r2, =board_array
    ldr r9, =player_turn
    
    ldr r0, [r1]
    mov r4, #4
    mul r1, r0, r4        @ convert inputed index with byte offset

    ldr r3, [r9]          @ load player
    str r3, [r2, r1]

    pop {r4-r12, lr}
    bx lr

@ parameters:
    @ none

@ returns:
    @ 1 on success, -1 on error
check_input:
    push {r4-r12, lr}

    ldr r0, =input_num     @ load user input
    ldr r1, [r0]

    cmp r1, #0             @ check if input is less than 0
    blt check_input_error  @ if so, branch to error

    cmp r1, #8             @ check if input is greater than 8
    bgt check_input_error

    ldr r0, =board_array
    lsl r2, r1, #2         @ convert inputed index with byte offset
    ldr r3, [r0, r2]       @ load board value

    cmp r3, #0
    bne check_input_error  @ if board value is not 0, branch to error

    no_error:
        mov r0, #1
        b check_input_end

    check_input_error:
        ldr r0, =input_error
        bl printf
        mov r0, #-1

    check_input_end:
        pop {r4-r12, lr}
        bx lr

@parameters:
    @ none

@returns:
    @ none
clear_buffer:
    push {r4-r12, lr}

    ldr r1, =player_input_buffer
    mov r2, #0                    @ r2 = i, counter
    mov r0, #0

    clear_buffer_loop:
        strb r0, [r1, r2]      @ set buffer value to 0s
        add r2, #1

        cmp r2, #16
        blt clear_buffer_loop  @ if i < 16, continue loop
    
    clear_buffer_end:
        pop {r4-r12, lr}
        bx lr

@ parameters:
    @ none
@ returns:
    @ none
clear_terminal:
        push {r4-r12, lr}

        mov r0, #1                  @ fd: stdout
        ldr r1, =clear_screen       
        ldr r2, =4                  
        mov r7, #4                  @ syscall: write
        svc 0

        pop {r4-r12, lr}
        bx lr
