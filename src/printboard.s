.data
@ array representing the board
    @ 0 = empty
    @ 1 = player 1 (X)
    @ 2 = player 2 (O)
    @ 9 = end of board
board_array: .word 0, 0, 1, 0, 2, 2, 0, 0, 0

@ strings for printing the board
board_line_01: .string " %s | %s | %s \n"  @ %s = board value
board_line_02: .string "---|---|---\n"

board_mark_01: .fill 2, 1, 0               @ storage for board values, to be replaced with x or o
board_mark_02: .fill 2, 1, 0
board_mark_03: .fill 2, 1, 0
@

.text
.global main
.func main

main:
    push {lr}
    @ do stuff
    ldr r0, =board_array
    ldr r1, =board_line_01
    ldr r2, =board_line_02
    bl print_board

    _end:
        pop {lr} 
        bx lr

@ parameters:
    @ r0 = board array address
    @ r1 = board_line_01 address
    @ r2 = board_line_02 address
print_board:
    push {lr}

    mov r8, #0                 @ r8 is counter for printing horizontal lines
    mov r9, #0                 @ r9 counts time for loops
    print_board_loop:
        cmp r9, #8             @ check for end of board
        bge print_board_end

        ldr r3, [r0], #4       @ load board value
        ldr r4, [r0], #4       @ load rest of board values
        ldr r5, [r0], #4


        print_board_line:
            push {r0-r3}
            ldr r1, =board_mark_01  @ set first board mark
            mov r2, r3
            mov r3, r9
            bl set_board_mark
            add r9, #1
            strb r0, [r1]
            mov r0, #0
            strb r0, [r1, #1]

            ldr r1, =board_mark_02  @ set second board mark
            mov r2, r4
            mov r3, r9
            bl set_board_mark
            add r9, #1
            strb r0, [r1]
            mov r0, #0
            strb r0, [r1, #1]

            ldr r1, =board_mark_03  @ set third board mark
            mov r2, r5
            mov r3, r9
            bl set_board_mark
            add r9, #1
            strb r0, [r1]
            mov r0, #0
            strb r0, [r1, #1]
            
            ldr r0, =board_line_01  @ set r0 to board_line_01 address, set r1, r2, r3 to board marks
            ldr r1, =board_mark_01
            ldr r2, =board_mark_02
            ldr r3, =board_mark_03
            bl printf               @ print board line
            aft_print_1:

        print_horizontal_line:
            cmp r8, #2              @ check if last line
            beq print_board_loop    @ skip horizontal line printing if last line
            ldr r0, =board_line_02  @ print horizontal line
            bl printf
            aft_print_2:

            add r8, #1              @ increment counter
            pop {r0-r3}             @ restore r0-r3
            b print_board_loop      @ continue loop

    print_board_end:
        pop {r0-r3}
        pop {lr}
        bx lr

@ parameters:
    @ r1 = board mark address
    @ r2 = current board value
    @ r3 = counter

@returns:
    @ r0 = board mark
set_board_mark:
    push {lr}

    cmp r2, #0
    bne not_blank
    add r0, r3, #'0'  @ convert the mark to the element of the array it is

    not_blank:
        cmp r2, #1
        moveq r0, #'X'

        cmp r2, #2
        moveq r0, #'O'

        pop {lr}
        bx lr
