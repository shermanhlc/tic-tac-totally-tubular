.equ MAX_INPUT_CHARS, 128
.data
board_array: .fill 9, 4, 0 @matrix governing board X's and O's
.balign 4
turn_counter: .word 0
player_turn: .word 1
turn_prompt: .string "Player %d's move: \n"
.balign 4
player_input_buffer: .space 16 @FIXED
non_number_dump: .space MAX_INPUT_CHARS
.balign 4
scan_format_parse: .string "%d %s"
.balign 4
input_num: .word 0
input_error: .string "Invalid input!\n"
.balign 4
@......

.text
.global main
.func main
main: push {r4-r12, lr}
	bl turn








	pop {r4-r12, lr}
	bx lr

turn: push {r4-r12, lr} @james wrote this subroutine @returns nothing!
	@@@@@@@bl print_board NEED SAMs CODE FOR THIS
	Prompt_player: bl clear_buffer @clear the buffer to avoid errors!
		ldr r0, =player_turn
		ldr r1, [r0] @r1 has whose turn it is
		ldr r0, =turn_prompt @r0 has the prompt address
		bl printf @prompts player to make a move
	Reading_Input: mov r0, #0 @std input
		ldr r1, =player_input_buffer
		mov r2, #(MAX_INPUT_CHARS - 1) @reserve a space for null char
		mov r7, #3 @read is syscall #3
		svc 0
	Parsing: ldr r0, =player_input_buffer
		ldr r1, =scan_format_parse
		ldr r2, =input_num @where to put the player input integer!
		ldr r3, =non_number_dump @the dump
		bl sscanf @r0 now contains amount of inputs 
	Check_input1: mov r10, r0 @save return value of sscanf
		cmp r10, #1 @ # of inputs should be 1
		ldrne r0, =input_error
		blne printf
		cmp r10, #1
		test1:
		bne Prompt_player @ask for another input
	Check_input2: bl check_input @another check with check_input function
		cmp r0, #0
		test2:
		blt Prompt_player @ask for another input
	@@@@@@@bl update_board
		
	Increment_turn:ldr r0, =turn_counter
		ldr r1, [r0]
		add r1, r1, #1 @increment turn
		str r1, [r0] @store it back
	Switch_Player: ldr r0, =player_turn @switches between Player1 and Player2
		ldr r1, [r0]
		cmp r1, #1
		moveq r1, #2
		beq end_switch
		mov r1, #1
		end_switch:
		ldr r1, [r0]
	pop {r4-r12, lr} @end subroutine
bx lr


check_input: push {r4-r12, lr} @returns -1 in r0 if theres an error
	ldr r0, =input_num
	ldr r1, [r0] @r1 has the input number
	cmp r1, #0
	blt error
	cmp r1, #8
	bgt error
	ldr r0, =board_array
	lsl r2, r1, #2 @r2 = r1 * 4
	ldr r3, [r0, r2] @r3 has board_array[4*input_num]
	cmp r3, #0
	bne error
	no_error: mov r0, #1
	b end_check_input
	error: ldr r0, =input_error
		bl printf
		mov r0, #-1
	end_check_input: pop {r4-r12, lr}
		bx lr
		
clear_buffer: push {r4-r12, lr} @void return
	ldr r4, =player_input_buffer
	mov r5, #0 @initialize counter i = 0
	mov r6, #0
	clear_loop: strb r6, [r4, r5]
		add r5, r5, #1 @i++
		cmp r5, #16
		blt clear_loop @do until i = 16
	end_clear_buffer: pop {r4-r12, lr}
		bx lr