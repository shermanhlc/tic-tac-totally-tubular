.data
@ array representing the board
    @ 0 = empty
    @ 1 = player 1 (X)
    @ 2 = player 2 (O)
    @ 9 = end of board
board_array: .word 0, 0, 1, 0, 1, 0, 1, 0, 0

@ strings for printing the board
board_line_01: .string " %s | %s | %s \n"  @ %s = board value
board_line_02: .string "---|---|---\n"

board_mark_01: .fill 2, 1, 0               @ storage for board values, to be replaced with x or o
board_mark_02: .fill 2, 1, 0
board_mark_03: .fill 2, 1, 0
@

.text
.global main
.func main

main:
    push {lr}
    @ do stuff
    ldr r0, =board_array
    bl check_winner

    ldr r0, =board_array
    bl print_board

    _end:
        pop {lr} 
        bx lr

@ parameters:
    @ none
    @ note: print_board should be called before the first call of turn

@ returns:
    @ none
turn:
    push {r4-r12, lr}

    prompt_player:
        ldr r0, =player_turn
        ldr r1, [r0]          @ load player turn to r1

        ldr r0, =turn_prompt
        bl printf             @ print turn prompt

    read_input:
        ldr r1, =player_input_buffer   @ load user input to buffer
        mov r2, #(MAX_INPUT_CHAR - 1)
        mov r7, #3                     @ syscall: read
        svc 0

    parse:
        ldr r0, =player_input_buffer   @ load user input
        ldr r1, =scan_format_parse
        ldr r2, =input_num             @ save the desired integer
        ldr r3, =non_number_dump       @ dump non-numbers

        bl sscanf                      @ parse input

    check_input_1:
        cmp r0, #1                     @ check if sscanf returned 1
        ldrne r0, =input_error
        blne printf                    @ print error message

        bne prompt_player              @ if sscanf did not return 1, branch to prompt_player

    check_input_2:
        bl check_input
        cmp r0, #0
        blt prompt_player              @ if check_input returned -1, branch to prompt_player

    bl update_board  @ update the board

    increment_turn:
        ldr r0, =turn_counter
        ldr r1, [r0]

        add r1, #1            @ increment turn counter
        str r1, [r0]          @ store turn counter

    switch_player:
        ldr r0, =player_turn
        ldr r1, [r0]

        cmp r1, #1
        moveq r1, #2
        movne r1, #1
        str r1, [r0]

        ldr r1, [r0]

    bl print_board   @ print the board

    pop {r4-r12, lr}
    bx lr


@ parameters:
    @ r0 = board array address
print_board:
    push {r4-r12, lr}

    mov r8, #0                 @ r8 is counter for printing horizontal lines
    mov r9, #0                 @ r9 counts time for loops
    print_board_loop:
        cmp r9, #8             @ check for end of board
        bge print_board_end

        ldr r3, [r0], #4       @ load board value
        ldr r4, [r0], #4       @ load rest of board values
        ldr r5, [r0], #4


        print_board_line:
            push {r0-r3}
            ldr r1, =board_mark_01  @ set first board mark
            mov r2, r3
            mov r3, r9
            bl set_board_mark
            add r9, #1
            strb r0, [r1]
            mov r0, #0
            strb r0, [r1, #1]

            ldr r1, =board_mark_02  @ set second board mark
            mov r2, r4
            mov r3, r9
            bl set_board_mark
            add r9, #1
            strb r0, [r1]
            mov r0, #0
            strb r0, [r1, #1]

            ldr r1, =board_mark_03  @ set third board mark
            mov r2, r5
            mov r3, r9
            bl set_board_mark
            add r9, #1
            strb r0, [r1]
            mov r0, #0
            strb r0, [r1, #1]
            
            ldr r0, =board_line_01  @ set r0 to board_line_01 address, set r1, r2, r3 to board marks
            ldr r1, =board_mark_01
            ldr r2, =board_mark_02
            ldr r3, =board_mark_03
            bl printf               @ print board line
            aft_print_1:

        print_horizontal_line:
            cmp r8, #2              @ check if last line
            beq print_board_loop    @ skip horizontal line printing if last line
            ldr r0, =board_line_02  @ print horizontal line
            bl printf
            aft_print_2:

            add r8, #1              @ increment counter
            pop {r0-r3}             @ restore r0-r3
            b print_board_loop      @ continue loop

    print_board_end:
        pop {r0-r3}
        pop {r4-12, lr}
        bx lr

@ parameters:
    @ r1 = board mark address
    @ r2 = current board value
    @ r3 = counter

@returns:
    @ r0 = board mark
set_board_mark:
    push {r4-r12, lr}

    cmp r2, #0
    bne not_blank
    add r0, r3, #'0'  @ convert the mark to the element of the array it is

    not_blank:
        cmp r2, #1
        moveq r0, #'X'

        cmp r2, #2
        moveq r0, #'O'

        pop {r4-12, lr}
        bx lr

@ parameters:
    @ r0 = board array address

@ returns:
    @ r0 = 1 if winner is found, 0 otherwise
check_winner:
    push {r4-12, lr}

    horizontal_check_1:
        ldr r1, [r0]      @ index: 0
        ldr r2, [r0, #4]  @ 1
        ldr r3, [r0, #8]  @ 2

        cmp r1, #0
        beq horizontal_check_2

        @ comparing via transitive property
        cmp r1, r2
        bne horizontal_check_2
        cmp r1, r3
        beq winner_found
    
    horizontal_check_2:
        ldr r1, [r0, #12]  @ 3
        ldr r2, [r0, #16]  @ 4
        ldr r3, [r0, #20]  @ 5

        cmp r1, #0
        beq horizontal_check_3

        @ comparing via transitive property
        cmp r1, r2
        bne horizontal_check_3
        cmp r1, r3
        beq winner_found

    horizontal_check_3:
        ldr r1, [r0, #24]  @ 6
        ldr r2, [r0, #28]  @ 7
        ldr r3, [r0, #32]  @ 8

        cmp r1, #0
        beq vertical_check_1

        @ comparing via transitive property
        cmp r1, r2
        bne vertical_check_1
        cmp r1, r3
        beq winner_found

    vertical_check_1:
        ldr r1, [r0]       @ 0
        ldr r2, [r0, #12]  @ 3
        ldr r3, [r0, #24]  @ 6

        cmp r1, #0
        beq vertical_check_2

        @ comparing via transitive property
        cmp r1, r2
        bne vertical_check_2
        cmp r1, r3
        beq winner_found
    
    vertical_check_2:
        ldr r1, [r0, #4]   @ 1
        ldr r2, [r0, #16]  @ 4
        ldr r3, [r0, #28]  @ 7

        cmp r1, #0
        beq vertical_check_3

        @ comparing via transitive property
        cmp r1, r2
        bne vertical_check_3
        cmp r1, r3
        beq winner_found

    vertical_check_3:
        ldr r1, [r0, #8]   @ 2
        ldr r2, [r0, #20]  @ 5
        ldr r3, [r0, #32]  @ 8

        cmp r1, #0
        beq diagonal_check_1

        @ comparing via transitive property
        cmp r1, r2
        bne diagonal_check_1
        cmp r1, r3
        beq winner_found

    diagonal_check_1:
        ldr r1, [r0]       @ 0
        ldr r2, [r0, #16]  @ 4
        ldr r3, [r0, #32]  @ 8

        cmp r1, #0
        beq diagonal_check_2

        @ comparing via transitive property
        cmp r1, r2
        bne diagonal_check_2
        cmp r1, r3
        beq winner_found

    diagonal_check_2:
        ldr r1, [r0, #8]   @ 2
        ldr r2, [r0, #16]  @ 4
        ldr r3, [r0, #24]  @ 6

        cmp r1, #0
        beq no_winner_found

        @ comparing via transitive property
        cmp r1, r2
        bne no_winner_found
        cmp r1, r3
        beq winner_found
    
    no_winner_found:
        mov r0, #0
        b check_winner_exit

    winner_found:
        mov r0, #1
        b check_winner_exit

    check_winner_exit:
        pop {r4-r12, lr}
        bx lr



@ parameters:
    @ none

@ returns:
    @ none
update_board:
    push {r4-r12, lr}

    ldr r1, =input_num    @ load addresses
    ldr r2, =board_array
    ldr r9, =player_turn
    
    ldr r0, [r1]
    mul r1, r0, #4        @ convert inputed index with byte offset

    ldr r3, [r9]          @ load player
    str r3, [r2, r1]

    pop {r4-r12, lr}
    bx lr

@ parameters:
    @ none

@ returns:
    @ -1 on error
check_input:
    push {r4-r12, lr}

    ldr r0, =input_num     @ load user input
    ldr r1, [r0]

    cmp r1, #0             @ check if input is less than 0
    blt check_input_error  @ if so, branch to error

    cmp r1, #8             @ check if input is greater than 8
    bgt check_input_error

    ldr r0, =board_array
    lsl r2, r1, #2         @ convert inputed index with byte offset
    ldr r3, [r0, r2]       @ load board value

    cmp r3, #0
    bne check_input_error  @ if board value is not 0, branch to error

@parameters:
    @ none

@returns:
    @ none
clear_buffer:
    push {r4-r12, lr}

    ldr r1, =player_input_buffer
    mov r2, #0                    @ r2 = i, counter
    mov r0, #0

    clear_buffer_loop:
        strb r0, [r1, r2]      @ set buffer value to 0s
        add r2, #1

        cmp r2, #16
        blt clear_buffer_loop  @ if i < 16, continue loop
    
    clear_buffer_end:
        pop {r4-r12, lr}
        bx lr
